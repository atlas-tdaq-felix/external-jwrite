# jWrite - JSON write library

simple and in c

https://www.codeproject.com/Articles/887604/jWrite-a-really-simple-JSON-writer-in-C

in subdir of jwrite:

cmake -DCMAKE_INSTALL_PREFIX=`pwd`/../../1.2.4/x86_64-centos9-gcc11-opt ..
make
make install

jwrite files were modified:
# 1.2.1
- const
- removed define
# 1.2.2
- functions added for uint, long and ulong
# 1.2.3
- changed isPretty into output and added JW_NDJSON
# 1.2.4
- changed jwrite.h to make it includable in c++ and c
